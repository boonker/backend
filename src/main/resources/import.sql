INSERT INTO roles(name) VALUES ('ROLE_USER');
INSERT INTO roles(name) VALUES ('ROLE_ADMIN');
INSERT INTO users(full_name, username, email, password, city, age, phone_number) VALUES ('Maja Vrsaljko', 'mvrsaljko', 'majavrsaljko78@gmail.com', '$2a$10$kOF.xsA1pU5K1qNjo/RpKOxdZecR1KYS7TjGJDijm8MuVbWmHKsWW', 'Rijeka', '23','0998861132');
INSERT INTO users(full_name, username, email, password, city, age, phone_number) VALUES ('Azra Subašić', 'asubasic', 'asubasic@gmail.com', '$2a$10$kOF.xsA1pU5K1qNjo/RpKOxdZecR1KYS7TjGJDijm8MuVbWmHKsWW', 'Rijeka', '22','099112356');
INSERT INTO users(full_name, username, email, password, city, age, phone_number) VALUES ('Ivana Baćac', 'ibacac', 'ibacac@gmail.com', '$2a$10$kOF.xsA1pU5K1qNjo/RpKOxdZecR1KYS7TjGJDijm8MuVbWmHKsWW', 'Grobnik', '24','099112344');
INSERT INTO users(full_name, username, email, password, city, age, phone_number) VALUES ('Luka Vukonić', 'lvukonic', 'lvukonic@gmail.com', '$2a$10$kOF.xsA1pU5K1qNjo/RpKOxdZecR1KYS7TjGJDijm8MuVbWmHKsWW', 'Zagreb', '30','099112345');
INSERT INTO users(full_name, username, email, password, city, age, phone_number) VALUES ('Romano Polić', 'rpolic', 'rpolic@gmail.com', '$2a$10$kOF.xsA1pU5K1qNjo/RpKOxdZecR1KYS7TjGJDijm8MuVbWmHKsWW', 'Rijeka', '22','099212345');
INSERT INTO users(full_name, username, email, password, city, age, phone_number) VALUES ('Hana Radojcic', 'hradojcic', 'hradojcic@gmail.com', '$2a$10$kOF.xsA1pU5K1qNjo/RpKOxdZecR1KYS7TjGJDijm8MuVbWmHKsWW', 'Rijeka', '22','099214345');
INSERT INTO user_roles(user_id, role_id) VALUES (1, 1);
INSERT INTO user_roles(user_id, role_id) VALUES (2, 1);
INSERT INTO user_roles(user_id, role_id) VALUES (3, 1);
INSERT INTO user_roles(user_id, role_id) VALUES (4, 1);
INSERT INTO user_roles(user_id, role_id) VALUES (5, 1);
INSERT INTO user_roles(user_id, role_id) VALUES (6, 1);
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Rijeka', 'Zadar', '1: Vozim za Zadar male pakete', '2021-09-09', '6', '100', 'Nudim prijevoz do Zd', 1, 'DRIVING', 'Ante Kovačića 2', '2021-09-07', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Rijeka', 'Zagreb', '2: Vozim za Zagreb veće pakete', '2021-09-10', '20', '50', 'Nudim prijevoz do Zg', 1, 'DRIVING', 'Ante Kovačića 2', '2021-09-05', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Rijeka', 'Split', 'Vozim za Split pošiljke', '2021-09-11', '15', '120', 'Nudim prijevoz do Splita', 1, 'DRIVING', 'Ante Kovačića 2', '2021-09-02', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, city_to_address, created_at, is_deleted) VALUES ('Rijeka', 'Delnice', 'Šaljem paket za Delnice', '2021-09-10', '15', '80', 'do Delnica', 2, 'SENDING', 'Rastočine 3', 'Delnice 2', '2021-09-07', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Rijeka', 'Delnice', 'Putujem za Rijeku POVOLJNO', '2021-09-12', '15', '200', 'Vozim za Rijeku', 2, 'DRIVING', 'Delnice 2', '2021-09-02', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Zagreb', 'Split', 'Vozim Zg-Sp', '2021-09-13', '1', '100', 'Vozim za Split manje pakete', 3, 'DRIVING', 'Dioklecijan 2', '2021-09-03', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, city_to_address, created_at, is_deleted) VALUES ('Benkovac', 'Osijek', 'Trazim za Osijek', '2021-09-10', '1', '100', 'Trazim prijevoz za paket do Osijeka', 4, 'SENDING', 'Ante Starcevic 10', 'Osiječka 2', '2021-09-05', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Rijeka', 'Mali Lošinj', 'Vozim', '2021-09-10', '1', '200', 'Vozim sve', 5, 'DRIVING', 'Osiječka 2', '2021-09-05', 'false');
INSERT INTO advertisement(city_from, city_to, content, delivery_date, package_size, price,title, user_id, advertisement_type, city_from_address, created_at, is_deleted) VALUES ('Rijeka', 'Benkovac', 'Vozim za Bnk', '2021-08-05', '50', '100', 'Nudim prijevoz do Benkovca', 1, 'DRIVING', 'Vukovarska 2', '2021-08-01', 'false');
INSERT INTO rating(published_on, review, score, created_by_id, created_for_id) VALUES ('2020-01-21', 'Brza dostava, paket je stigao neostecen!', 5, 3, 1);
INSERT INTO rating(published_on, review, score, created_by_id, created_for_id) VALUES ('2020-08-03', 'Dostava je kasnila!!', 3, 4, 1);
INSERT INTO rating(published_on, review, score, created_by_id, created_for_id) VALUES ('2020-09-01', 'Ljubazna i pristojna', 4, 5, 1);
INSERT INTO rating(published_on, review, score, created_by_id, created_for_id) VALUES ('2020-08-12', 'Sve je prošlo super!!', 5, 5, 2);
INSERT INTO rating(published_on, review, score, created_by_id, created_for_id) VALUES ('2020-08-22', 'Paket je oštećen!!', 1, 4, 2);
INSERT INTO rating(published_on, review, score, created_by_id, created_for_id) VALUES ('2020-09-01', 'Brzo i efikasno, sve prema dogovoru', 5, 6, 2);
INSERT INTO users_following(user_id, following_id) VALUES (1, 4);
INSERT INTO users_followers(user_id, followers_id) VALUES(4, 1);
INSERT INTO users_following(user_id, following_id) VALUES (1, 5);
INSERT INTO users_followers(user_id, followers_id) VALUES(5, 1);
INSERT INTO users_following(user_id, following_id) VALUES (4, 1);
INSERT INTO users_followers(user_id, followers_id) VALUES(1, 4);
INSERT INTO users_following(user_id, following_id) VALUES (5, 1);
INSERT INTO users_followers(user_id, followers_id) VALUES(1, 5);
INSERT INTO users_following(user_id, following_id) VALUES (6, 1);
INSERT INTO users_followers(user_id, followers_id) VALUES(1, 6);
INSERT INTO users_following(user_id, following_id) VALUES (2, 3);
INSERT INTO users_followers(user_id, followers_id) VALUES(3, 2);
INSERT INTO users_following(user_id, following_id) VALUES (2, 4);
INSERT INTO users_followers(user_id, followers_id) VALUES(4, 2);
INSERT INTO users_following(user_id, following_id) VALUES (2, 5);
INSERT INTO users_followers(user_id, followers_id) VALUES(5, 2);
INSERT INTO users_following(user_id, following_id) VALUES (2, 6);
INSERT INTO users_followers(user_id, followers_id) VALUES(6, 2);
INSERT INTO users_following(user_id, following_id) VALUES (3, 2);
INSERT INTO users_followers(user_id, followers_id) VALUES(2, 3);
INSERT INTO users_following(user_id, following_id) VALUES (4, 2);
INSERT INTO users_followers(user_id, followers_id) VALUES(2, 4);
INSERT INTO users_following(user_id, following_id) VALUES (5, 2);
INSERT INTO users_followers(user_id, followers_id) VALUES(2, 5);
INSERT INTO offers(address, message, package_size, advertisement_id, created_by_id, offer_type, published_on) VALUES ('Put Plovanije 3', 'Može prijevoz manjeg paketa', 2,  1, 2, 'ACCEPTED', '2021-09-08')
INSERT INTO offers(address, message, package_size, advertisement_id, created_by_id, offer_type, published_on) VALUES ('Ulica 13', 'Pozdrav, može prijevoz kauča', 20,  1, 3, 'DECLINED', '2021-09-08')
INSERT INTO offers(address, message, package_size, advertisement_id, created_by_id, offer_type, published_on) VALUES ('Ulica 10', 'Pozdrav, može prijevoz bicikle', 10,  1, 4, 'PENDING', '2021-09-08')
INSERT INTO offers(address, message, package_size, advertisement_id, created_by_id, offer_type, published_on) VALUES ('Ulica 9', 'Htio bi poslati paket igrački', 5,  2, 5, 'PENDING', '2021-09-08')
INSERT INTO offers(address, message, package_size, advertisement_id, created_by_id, offer_type, published_on) VALUES ('Ulica 8', 'Htjela bi poslati paket šminke', 8,  9, 6, 'ACCEPTED', '2021-08-02')
INSERT INTO offers(address, message, package_size, advertisement_id, created_by_id, offer_type, published_on) VALUES ('Ulica 12', 'Htjela bi poslati namještaj', 28,  9, 3, 'ACCEPTED', '2021-08-02')
INSERT INTO deliveries(created_on, delivery_status, delivery_type, for_user_id, offer_id, user_id, delivery_date) VALUES('2021-09-05', 'FINISHED', 'DRIVING', 6, 5, 1, '2021-09-05')
INSERT INTO deliveries(created_on, delivery_status, delivery_type, for_user_id, offer_id, user_id, delivery_date) VALUES('2021-09-05', 'PAST', 'DRIVING', 3, 5, 1, '2021-09-05')
INSERT INTO deliveries(created_on, delivery_status, delivery_type, for_user_id, offer_id, user_id, delivery_date) VALUES('2021-09-09', 'INCOMING', 'DRIVING', 2, 1, 1, '2021-09-09')