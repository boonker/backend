package hr.riteh.boonker.repository;

import hr.riteh.boonker.model.Advertisement;
import hr.riteh.boonker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {

    List<Advertisement> findAllByUserAndIsDeletedFalseOrderByCreatedAtDesc(User user);

    List<Advertisement> findAllByCityFromAndIsDeletedFalse(String cityFrom);

}