package hr.riteh.boonker.repository;

import hr.riteh.boonker.model.Delivery;
import hr.riteh.boonker.model.DeliveryType;
import hr.riteh.boonker.model.Offer;
import hr.riteh.boonker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeliveryRepository extends JpaRepository<Delivery, Long> {

    List<Delivery> findAllByDeliveryTypeAndUserOrderByDeliveryDateDesc(DeliveryType deliveryType, User user);

    List<Delivery> findAllByDeliveryTypeAndForUser(DeliveryType deliveryType, User userFor);

    List<Delivery> findAllByOffer(Offer offer);
}
