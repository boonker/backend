package hr.riteh.boonker.repository;

import hr.riteh.boonker.model.Advertisement;
import hr.riteh.boonker.model.Offer;
import hr.riteh.boonker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfferRepository extends JpaRepository<Offer, Long> {

    List<Offer> findAllByAdvertisementOrderByPublishedOn(Advertisement advertisement);

    List<Offer> findAllByCreatedBy(User creator);
}
