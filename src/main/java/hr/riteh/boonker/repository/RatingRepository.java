package hr.riteh.boonker.repository;

import hr.riteh.boonker.model.Rating;
import hr.riteh.boonker.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {

    List<Rating> findAllByCreatedForOrderByPublishedOnDesc(User user);
}
