package hr.riteh.boonker.mapper;

import hr.riteh.boonker.controller.response.DeliveryResponse;
import hr.riteh.boonker.model.Delivery;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface DeliveryMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.phoneNumber", target = "userPhoneNumber")
    @Mapping(source = "user.fullName", target = "userFullName")
    @Mapping(source = "forUser.id", target = "forUserId")
    @Mapping(source = "forUser.phoneNumber", target = "forUserPhoneNumber")
    @Mapping(source = "forUser.fullName", target = "forUserFullName")
    @Mapping(source = "offer.advertisement.cityFrom", target = "cityFrom")
    @Mapping(source = "offer.advertisement.cityFromAddress", target = "cityFromAddress")
    @Mapping(source = "offer.advertisement.cityTo", target = "cityTo")
    @Mapping(source = "offer.address", target = "cityToAddress")
    @Mapping(source = "offer.message", target = "message")
    @Mapping(source = "offer.advertisement.price", target = "price")
    DeliveryResponse deliveryToDeliveryResponse(Delivery source);

    List<DeliveryResponse> deliveryListToDeliveryResponseList(List<Delivery> deliveryList);
}
