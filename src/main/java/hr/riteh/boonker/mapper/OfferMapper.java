package hr.riteh.boonker.mapper;

import hr.riteh.boonker.controller.request.NewOfferRequest;
import hr.riteh.boonker.controller.response.OfferResponse;
import hr.riteh.boonker.model.Offer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;

@Mapper
public interface OfferMapper {

    Offer newOfferRequestToOffer(NewOfferRequest newOfferRequest);

    @Mapping(target = "createdById", source = "createdBy.id")
    @Mapping(target = "createdByFullName", source = "createdBy.fullName")
    @Mapping(target = "advertisementId", source = "advertisement.id")
    @Mapping(target = "id", source = "id")
    OfferResponse offerToOfferResponse(Offer offer);

    List<OfferResponse> offerListToOfferResponseList(Collection<Offer> offerList);

    default Long stringToLong(String value) {
        if(value.isBlank()) return 0L;
        else return Long.valueOf(value);
    }
}
