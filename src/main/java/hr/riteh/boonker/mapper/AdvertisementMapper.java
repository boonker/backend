package hr.riteh.boonker.mapper;

import hr.riteh.boonker.controller.request.NewAdvertisementRequest;
import hr.riteh.boonker.controller.response.AdvertisementResponse;
import hr.riteh.boonker.model.Advertisement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface AdvertisementMapper {

    @Mapping(target = "cityFrom", source = "cityFrom")
    @Mapping(target = "cityTo", source = "cityTo")
    @Mapping(target = "deliveryDate", source = "deliveryDate")
    @Mapping(target = "packageSize", source = "packageSize")
    Advertisement newAdvertisementRequestToAdvertisement(NewAdvertisementRequest newAdvertisementRequest);

    @Mapping(target = "cityFrom", source = "cityFrom")
    @Mapping(target = "cityTo", source = "cityTo")
    @Mapping(target = "deliveryDate", source = "deliveryDate")
    @Mapping(target = "packageSize", source = "packageSize")
    @Mapping(target = "createdAt", source = "createdAt")
    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "userFullName", source = "user.fullName")
    @Mapping(target = "userPhoneNumber", source = "user.phoneNumber")
    AdvertisementResponse advertisementToAdvertisementResponse(Advertisement advertisement);

    List<AdvertisementResponse> advertisementListToAdvertisementResponseList(List<Advertisement> advertisements);
}
