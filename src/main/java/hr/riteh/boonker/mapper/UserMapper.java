package hr.riteh.boonker.mapper;

import hr.riteh.boonker.controller.response.PersonalInfoResponse;
import hr.riteh.boonker.controller.response.UserResponse;
import hr.riteh.boonker.model.User;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

    UserResponse userToUserResponse(User user);

    PersonalInfoResponse userToPersonalInfoResponse(User user);
}
