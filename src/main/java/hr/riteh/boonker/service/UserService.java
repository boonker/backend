package hr.riteh.boonker.service;

import hr.riteh.boonker.controller.request.ExpoPushTokenRequest;
import hr.riteh.boonker.controller.request.UpdatePersonalInfo;
import hr.riteh.boonker.controller.response.*;
import hr.riteh.boonker.mapper.AdvertisementMapper;
import hr.riteh.boonker.mapper.UserMapper;
import hr.riteh.boonker.model.Rating;
import hr.riteh.boonker.model.User;
import hr.riteh.boonker.repository.AdvertisementRepository;
import hr.riteh.boonker.repository.RatingRepository;
import hr.riteh.boonker.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class UserService {

    private final AdvertisementRepository advertisementRepository;

    private final UserRepository userRepository;

    private final RatingRepository ratingRepository;

    private final UserMapper userMapper;

    private final AdvertisementMapper advertisementMapper;

    private final PasswordEncoder encoder;

    public Optional<User> getCurrentLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();

            return userRepository.findByUsername(username);
        } else {
            log.error("No such user");
        }

        return Optional.empty();
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public UserResponse getUserDetails(String id) {
        Optional<User> currentUser = getCurrentLoggedInUser();

        Optional<User> user = userRepository.findById(Long.valueOf(id));

        if (user.isPresent()) {
            UserResponse userResponse = userMapper.userToUserResponse(user.get());
            List<Rating> ratings = ratingRepository.findAllByCreatedForOrderByPublishedOnDesc(user.get());

            long totalScore = ratings.stream()
                    .mapToLong(Rating::getScore)
                    .sum();
            if (totalScore != 0L) {
                double totalRating = (double) (totalScore / ratings.size());
                userResponse.setTotalRatingScore(String.valueOf(totalRating));
            } else {
                userResponse.setTotalRatingScore(String.valueOf(0L));
            }

            userResponse.setTotalFollowersNumber(String.valueOf(user.get().getFollowers().size()));
            userResponse.setTotalFollowingNumber(String.valueOf(user.get().getFollowing().size()));

            if (currentUser.isPresent()) {
                List<User> followingUsers = currentUser.get().getFollowing();

                userResponse.setIsFollowed(followingUsers.contains(user.get()));
            }
            return userResponse;
        }
        return null;
    }

    public List<AdvertisementResponse> getUserAdvertisement(String id) {
        Optional<User> user = userRepository.findById(Long.valueOf(id));

        if (user.isPresent()) {
            return advertisementMapper.advertisementListToAdvertisementResponseList(advertisementRepository.findAllByUserAndIsDeletedFalseOrderByCreatedAtDesc(user.get()));
        }

        return Collections.emptyList();
    }

    public void followUser(String id) {
        Optional<User> user = getCurrentLoggedInUser();

        if (user.isPresent()) {
            Optional<User> userToFollow = userRepository.findById(Long.valueOf(id));

            if (userToFollow.isPresent()) {
                List<User> following = user.get().getFollowing();
                following.add(userToFollow.get());

                List<User> userToFollowFollowersList = userToFollow.get().getFollowers();
                userToFollowFollowersList.add(user.get());

                userRepository.save(user.get());
                userRepository.save(userToFollow.get());
            }
        }
    }

    public void unfollowUser(String id) {
        Optional<User> user = getCurrentLoggedInUser();

        if (user.isPresent()) {
            Optional<User> userToUnfollow = userRepository.findById(Long.valueOf(id));

            if (userToUnfollow.isPresent()) {
                List<User> following = user.get().getFollowing();
                List<User> followingFiltered = following.stream()
                        .filter(followingUser -> !followingUser.getId().equals(userToUnfollow.get().getId()))
                        .collect(Collectors.toList());
                user.get().setFollowing(followingFiltered);

                List<User> userToUnfollowFollowersList = userToUnfollow.get().getFollowers();
                List<User> userToUnfollowFollowersListFiltered = userToUnfollowFollowersList.stream()
                        .filter(followersUser -> !followersUser.getId().equals(user.get().getId()))
                        .collect(Collectors.toList());
                userToUnfollow.get().setFollowers(userToUnfollowFollowersListFiltered);

                userRepository.save(user.get());
                userRepository.save(userToUnfollow.get());
            }
        }
    }

    public List<UserFollowResponse> getUserFollowers(String id) {
        Optional<User> user = getCurrentLoggedInUser();

        if (user.isPresent()) {
            Optional<User> userToCheck = userRepository.findById(Long.valueOf(id));

            if (userToCheck.isPresent()) {
                List<User> followersToCheck = userToCheck.get().getFollowers();
                List<User> userFollowing = user.get().getFollowing();

                return followersToCheck.stream().map(followedUser -> {
                    UserFollowResponse userFollowResponse = new UserFollowResponse();

                    userFollowResponse.setUserResponse(userMapper.userToUserResponse(followedUser));
                    userFollowResponse.setIsFollowedByCurrentUser(userFollowing.contains(followedUser));

                    return userFollowResponse;
                })
                        .collect(Collectors.toList());
            }

        }
        return Collections.emptyList();
    }

    public List<UserFollowResponse> getUserFollowing(String id) {
        Optional<User> user = getCurrentLoggedInUser();

        if (user.isPresent()) {
            Optional<User> userToCheck = userRepository.findById(Long.valueOf(id));

            if (userToCheck.isPresent()) {
                List<User> followingToCheck = userToCheck.get().getFollowing();
                List<User> userFollowing = user.get().getFollowing();

                return followingToCheck.stream().map(followingUser -> {
                    UserFollowResponse userFollowResponse = new UserFollowResponse();

                    userFollowResponse.setUserResponse(userMapper.userToUserResponse(followingUser));
                    userFollowResponse.setIsFollowedByCurrentUser(userFollowing.contains(followingUser));

                    return userFollowResponse;
                })
                        .collect(Collectors.toList());
            }

        }
        return Collections.emptyList();
    }

    public void setExpoPushToken(ExpoPushTokenRequest expoPushTokenRequest) {
        Optional<User> user = getCurrentLoggedInUser();

        if (user.isPresent()) {
            user.get().setExpoPushToken(expoPushTokenRequest.getExpoPushToken());
            userRepository.save(user.get());
        } else {
            throw new ResourceNotFoundException("no such user");
        }
    }

    public ExpoPushTokenResponse getExpoPushToken(String userId) {
        Optional<User> user = userRepository.findById(Long.valueOf(userId));

        ExpoPushTokenResponse expoPushTokenResponse = new ExpoPushTokenResponse();

        if (user.isPresent()) {
            expoPushTokenResponse.setExpoPushToken(user.get().getExpoPushToken());
            expoPushTokenResponse.setUserId(user.get().getId().toString());
            expoPushTokenResponse.setUserFullName(user.get().getFullName());
        }

        return expoPushTokenResponse;
    }

    public PersonalInfoResponse getPersonalInfoResponse() {
        var user = getCurrentLoggedInUser();
        if (user.isPresent()) {
            var personalInfo = userMapper.userToPersonalInfoResponse(user.get());

            return personalInfo;
        }

        return new PersonalInfoResponse();
    }

    public void updatePersonalInfo(UpdatePersonalInfo updatePersonalInfo) {
        var user = getCurrentLoggedInUser();
        if (user.isPresent()) {
            user.get().setFullName(updatePersonalInfo.getFullName());
            user.get().setAge(updatePersonalInfo.getAge());
            user.get().setCity(updatePersonalInfo.getCity());
            user.get().setPhoneNumber(updatePersonalInfo.getPhoneNumber());

            if (!updatePersonalInfo.getPassword().isBlank() || !updatePersonalInfo.getPassword().isEmpty()) {
                user.get().setPassword(encoder.encode(updatePersonalInfo.getPassword()));
            }

            userRepository.save(user.get());
        }
    }
}
