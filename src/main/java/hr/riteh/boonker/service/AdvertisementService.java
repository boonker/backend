package hr.riteh.boonker.service;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import hr.riteh.boonker.controller.request.NewAdvertisementRequest;
import hr.riteh.boonker.controller.request.SearchAdvertisementQueryRequest;
import hr.riteh.boonker.controller.response.AdvertisementResponse;
import hr.riteh.boonker.mapper.AdvertisementMapper;
import hr.riteh.boonker.model.*;
import hr.riteh.boonker.repository.AdvertisementRepository;
import hr.riteh.boonker.repository.OfferRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class AdvertisementService {

    private final AdvertisementRepository advertisementRepository;

    private final OfferRepository offerRepository;

    private final AdvertisementMapper mapper;

    private final UserService userService;

    private final EntityManager entityManager;

    public void newAdvertisement(NewAdvertisementRequest newAdvertisementRequest) {
        Advertisement advertisement = mapper.newAdvertisementRequestToAdvertisement(newAdvertisementRequest);
        Optional<User> user = userService.getCurrentLoggedInUser();

        user.ifPresent(advertisement::setUser);
        advertisement.setAdvertisementType(AdvertisementType.valueOf(newAdvertisementRequest.getAdvertisementType()));
        advertisement.setCreatedAt(LocalDate.now().format(DateTimeFormatter.ofPattern("yyy-MM-dd")));
        advertisement.setIsDeleted(false);

        advertisementRepository.save(advertisement);
    }

    public AdvertisementResponse getAdvertisement(Long id) {
        return mapper.advertisementToAdvertisementResponse(advertisementRepository.findById(id).get());
    }

    public void deleteAdvertisement(Long id) {
        Optional<Advertisement> advertisement = advertisementRepository.findById(id);

        if (advertisement.isPresent()) {
            advertisement.get().setIsDeleted(true);

            List<Offer> offers = offerRepository.findAllByAdvertisementOrderByPublishedOn(advertisement.get());

            offers.forEach(offer -> {
                if (offer.getOfferType().equals(OfferType.PENDING)) {
                    offer.setOfferType(OfferType.CANCELED);
                    offerRepository.save(offer);
                }
            });

            advertisementRepository.save(advertisement.get());
        }
    }

    @Transactional
    public void updateAdvertisement(Long id, NewAdvertisementRequest newAdvertisementRequest) {
        Optional<Advertisement> oldAdvertisement = advertisementRepository.findById(id);

        if (oldAdvertisement.isPresent()) {
            oldAdvertisement.get().setTitle(newAdvertisementRequest.getTitle());
            oldAdvertisement.get().setContent(newAdvertisementRequest.getContent());
            oldAdvertisement.get().setPrice(Long.valueOf(newAdvertisementRequest.getPrice()));
            oldAdvertisement.get().setCityFrom(newAdvertisementRequest.getCityFrom());
            oldAdvertisement.get().setCityTo(newAdvertisementRequest.getCityTo());
            oldAdvertisement.get().setDeliveryDate(newAdvertisementRequest.getDeliveryDate());
            oldAdvertisement.get().setPackageSize(newAdvertisementRequest.getPackageSize());
            advertisementRepository.save(oldAdvertisement.get());
        }
    }

    public List<Advertisement> allUserAdvertisement() {
        Optional<User> user = userService.getCurrentLoggedInUser();

        if (user.isPresent()) {
            return advertisementRepository.findAllByUserAndIsDeletedFalseOrderByCreatedAtDesc(user.get());
        }

        return Collections.emptyList();
    }

    public List<AdvertisementResponse> getAllAdvertisements(SearchAdvertisementQueryRequest queryRequest) {
        List<Advertisement> advertisements = getAllAdvertisementsBySearchAdvertisementQueryRequest(queryRequest);

        return mapper.advertisementListToAdvertisementResponseList(advertisements);
    }

    private List<Advertisement> getAllAdvertisementsBySearchAdvertisementQueryRequest(SearchAdvertisementQueryRequest queryRequest) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        QAdvertisement advertisement = QAdvertisement.advertisement;

        BooleanBuilder booleanBuilder = new BooleanBuilder();

        if (!queryRequest.getCityFrom().isBlank()) {
            var cityFromPredicate = advertisement.cityFrom.like(queryRequest.getCityFrom());
            booleanBuilder.and(cityFromPredicate);
        }

        if (!queryRequest.getCityTo().isBlank()) {
            var cityToPredicate = advertisement.cityTo.like(queryRequest.getCityTo());
            booleanBuilder.and(cityToPredicate);
        }

        if (!queryRequest.getAdvertisementType().isBlank()) {
            var advertisementTypePredicate = advertisement.advertisementType.eq(AdvertisementType.valueOf(queryRequest.getAdvertisementType()));
            booleanBuilder.and(advertisementTypePredicate);
        } else {
            var advertisementTypePredicate = advertisement.advertisementType.eq(AdvertisementType.SENDING).or(advertisement.advertisementType.eq(AdvertisementType.DRIVING));
            booleanBuilder.and(advertisementTypePredicate);
        }

        if (queryRequest.getFollowedByUser()) {
            Optional<User> user = userService.getCurrentLoggedInUser();
            if (user.isPresent()) {
                var userFollowedPredicate = advertisement.user.in(user.get().getFollowing());
                booleanBuilder.and(userFollowedPredicate);
            }
        }

        if (!queryRequest.getDeliveryDate().isBlank()) {
            var deliveryDatePredicate = advertisement.deliveryDate.eq(queryRequest.getDeliveryDate());
            booleanBuilder.and(deliveryDatePredicate);
        }

        if (!queryRequest.getUserFullName().isBlank()) {
            var userFullNamePredicate = advertisement.user.fullName.like("%" + queryRequest.getUserFullName() + "%");
            booleanBuilder.and(userFullNamePredicate);
        }

        var isNotDeletedPredicate = advertisement.isDeleted.isFalse();
        booleanBuilder.and(isNotDeletedPredicate);

        return queryFactory.selectFrom(advertisement)
                .where(booleanBuilder)
                .orderBy(queryRequest.getSortBy().equals("") ?
                        advertisement.createdAt.desc() :
                        queryRequest.getSortBy().equals(SortAdvertisement.LOW_TO_HIGH_PRICE.toString()) ?
                                advertisement.price.asc() : advertisement.price.desc())
                .limit(500)
                .fetch();
    }
}
