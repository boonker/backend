package hr.riteh.boonker.service;

import hr.riteh.boonker.controller.request.NewRatingRequest;
import hr.riteh.boonker.model.Rating;
import hr.riteh.boonker.model.User;
import hr.riteh.boonker.repository.RatingRepository;
import hr.riteh.boonker.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class RatingsService {

    private final UserService userService;

    private final RatingRepository ratingRepository;

    private final UserRepository userRepository;

    public List<Rating> getUserRatings(Long userId) {
        Optional<User> user = userRepository.findById(userId);

        if (user.isPresent()) {
            return ratingRepository.findAllByCreatedForOrderByPublishedOnDesc(user.get());
        }

        return Collections.emptyList();
    }

    public void newUserRatings(NewRatingRequest newRatingRequest) {
        Optional<User> createdForUser = userRepository.findById(Long.valueOf(newRatingRequest.getForUserId()));
        Optional<User> currentLoggedInUser = userService.getCurrentLoggedInUser();

        if (currentLoggedInUser.isPresent() && createdForUser.isPresent()) {
            Rating rating = new Rating();

            rating.setCreatedBy(currentLoggedInUser.get());
            rating.setCreatedFor(createdForUser.get());
            rating.setPublishedOn(LocalDate.now());
            rating.setScore(newRatingRequest.getScore());
            rating.setReview(newRatingRequest.getReview());

            ratingRepository.save(rating);
        }

    }
}
