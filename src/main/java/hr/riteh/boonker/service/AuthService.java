package hr.riteh.boonker.service;

import hr.riteh.boonker.controller.request.LoginRequest;
import hr.riteh.boonker.controller.request.RegistrationRequest;
import hr.riteh.boonker.controller.response.JwtResponse;
import hr.riteh.boonker.controller.response.MessageResponse;
import hr.riteh.boonker.model.Role;
import hr.riteh.boonker.model.RoleType;
import hr.riteh.boonker.model.User;
import hr.riteh.boonker.repository.RoleRepository;
import hr.riteh.boonker.repository.UserRepository;
import hr.riteh.boonker.security.jwt.JwtUtils;
import hr.riteh.boonker.security.service.impl.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder encoder;

    private final JwtUtils jwtUtils;

    public JwtResponse authenticateUser(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
                userDetails.getEmail(), roles, userDetails.getFullName());
    }

    public List<MessageResponse> registerUser(RegistrationRequest registrationRequest) {
        List<MessageResponse> messageResponses = new java.util.ArrayList<>();
        boolean userExists = false;

        if (userRepository.existsByUsername(registrationRequest.getUsername())) {
            userExists = true;
            messageResponses.add(new MessageResponse("Username is already taken!"));
        }

        if (userRepository.existsByEmail(registrationRequest.getEmail())) {
            userExists = true;
            messageResponses.add(new MessageResponse("Email is already in use!"));
        }

        if (userRepository.existsByPhoneNumber(registrationRequest.getPhoneNumber())) {
            userExists = true;
            messageResponses.add(new MessageResponse("Phone number is already in use!"));
        }

        if (userExists) {
            return messageResponses;
        }

        User user = new User(registrationRequest.getFullName(), registrationRequest.getUsername(),
                registrationRequest.getEmail(), encoder.encode(registrationRequest.getPassword()),
                registrationRequest.getCity(), registrationRequest.getAge(), registrationRequest.getPhoneNumber());

        Set<String> strRoles = registrationRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(RoleType.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                if ("admin".equals(role)) {
                    Role adminRole = roleRepository.findByName(RoleType.ROLE_ADMIN)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(adminRole);
                } else {
                    Role userRole = roleRepository.findByName(RoleType.ROLE_USER)
                            .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                    roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return Collections.emptyList();
    }

}
