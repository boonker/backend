package hr.riteh.boonker.service;

import hr.riteh.boonker.controller.response.DeliveryResponse;
import hr.riteh.boonker.mapper.DeliveryMapper;
import hr.riteh.boonker.model.Delivery;
import hr.riteh.boonker.model.DeliveryStatus;
import hr.riteh.boonker.model.DeliveryType;
import hr.riteh.boonker.model.User;
import hr.riteh.boonker.repository.DeliveryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class DeliveryService {

    private final UserService userService;

    private final DeliveryRepository deliveryRepository;

    private final DeliveryMapper deliveryMapper;

    public DeliveryResponse getDelivery(String deliveryId) {
        Optional<Delivery> delivery = deliveryRepository.findById(Long.valueOf(deliveryId));

        return delivery.map(deliveryMapper::deliveryToDeliveryResponse).orElse(null);
    }

    public List<DeliveryResponse> getDrivingDeliveriesResponse() {
        Optional<User> user = userService.getCurrentLoggedInUser();

        if (user.isPresent()) {
            List<Delivery> deliveryList = deliveryRepository.findAllByDeliveryTypeAndUserOrderByDeliveryDateDesc(DeliveryType.DRIVING, user.get());

            setDeliveriesStatus(deliveryList);

            List<Delivery> updatedDeliveryList = deliveryRepository.findAllByDeliveryTypeAndUserOrderByDeliveryDateDesc(DeliveryType.DRIVING, user.get());

            return deliveryMapper.deliveryListToDeliveryResponseList(updatedDeliveryList);
        }
        return Collections.emptyList();
    }

    public void setDeliveriesStatus(List<Delivery> deliveryList) {
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyy-MM-dd"));

        deliveryList.forEach(delivery -> {
            if(delivery.getDeliveryDate().equals(today) && !delivery.getDeliveryStatus().equals(DeliveryStatus.ACTIVE)) {
                delivery.setDeliveryStatus(DeliveryStatus.TODAY);
                deliveryRepository.save(delivery);
            } else if(delivery.getDeliveryDate().compareTo(today) < 0 && !delivery.getDeliveryStatus().equals(DeliveryStatus.PAST) && !delivery.getDeliveryStatus().equals(DeliveryStatus.FINISHED)) {
                delivery.setDeliveryStatus(DeliveryStatus.PAST);
                deliveryRepository.save(delivery);
            }
        });
    }

    public List<DeliveryResponse> getIncomingDeliveriesResponse() {
        Optional<User> user = userService.getCurrentLoggedInUser();

        if(user.isPresent()) {
            List<Delivery> deliveryList = deliveryRepository.findAllByDeliveryTypeAndForUser(DeliveryType.DRIVING, user.get());

            setDeliveriesStatus(deliveryList);

            List<Delivery> updatedDeliveryList = deliveryRepository.findAllByDeliveryTypeAndForUser(DeliveryType.DRIVING, user.get());

            return deliveryMapper.deliveryListToDeliveryResponseList(updatedDeliveryList);
        }

        return Collections.emptyList();
    }

    public void setActiveDelivery(String deliveryId) {
        Optional<Delivery> delivery = deliveryRepository.findById(Long.valueOf(deliveryId));

        delivery.ifPresent(value -> {
            value.setDeliveryStatus(DeliveryStatus.ACTIVE);
            deliveryRepository.save(value);
        });
    }

    public void setFinishedDelivery(String deliveryId) {
        Optional<Delivery> delivery = deliveryRepository.findById(Long.valueOf(deliveryId));

        delivery.ifPresent(value -> {
            value.setDeliveryStatus(DeliveryStatus.FINISHED);
            deliveryRepository.save(value);
        });
    }
}
