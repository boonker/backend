package hr.riteh.boonker.service;

import hr.riteh.boonker.controller.request.NewOfferRequest;
import hr.riteh.boonker.controller.response.AdvertisementOfferResponse;
import hr.riteh.boonker.controller.response.OfferResponse;
import hr.riteh.boonker.mapper.OfferMapper;
import hr.riteh.boonker.model.*;
import hr.riteh.boonker.repository.AdvertisementRepository;
import hr.riteh.boonker.repository.DeliveryRepository;
import hr.riteh.boonker.repository.OfferRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class OfferService {

    private final OfferMapper offerMapper;

    private final UserService userService;

    private final AdvertisementRepository advertisementRepository;

    private final OfferRepository offerRepository;

    private final DeliveryRepository deliveryRepository;

    public void newOffer(NewOfferRequest newOfferRequest) {
        Optional<User> user = userService.getCurrentLoggedInUser();
        Optional<Advertisement> advertisement = advertisementRepository.findById(Long.valueOf(newOfferRequest.getAdvertisementId()));

        Offer offer = offerMapper.newOfferRequestToOffer(newOfferRequest);

        if (user.isPresent() && advertisement.isPresent()) {
            offer.setCreatedBy(user.get());
            offer.setAdvertisement(advertisement.get());
            offer.setPublishedOn(LocalDate.now());
            offer.setOfferType(OfferType.PENDING);

            offerRepository.save(offer);
        }
    }

    public AdvertisementOfferResponse getAdvertisementOffers(String advertisementId) {
        Optional<Advertisement> advertisement = advertisementRepository.findById(Long.valueOf(advertisementId));

        if (advertisement.isPresent()) {
            List<Offer> offerList = offerRepository.findAllByAdvertisementOrderByPublishedOn(advertisement.get());

            List<OfferResponse> offerResponses = offerMapper.offerListToOfferResponseList(offerList);

            Long reservedBoonkerSize = offerResponses.stream()
                    .filter(offerResponse -> offerResponse.getOfferType().equals(OfferType.ACCEPTED))
                    .mapToLong(OfferResponse::getPackageSize).sum();

            AdvertisementOfferResponse advertisementOfferResponse = new AdvertisementOfferResponse();
            advertisementOfferResponse.setOffers(offerResponses);
            advertisementOfferResponse.setReservedBoonkerSize(reservedBoonkerSize);
            advertisementOfferResponse.setAdvertisedBoonkerSize(Long.valueOf(advertisement.get().getPackageSize()));
            advertisementOfferResponse.setAdvertisementType(advertisement.get().getAdvertisementType());

            return advertisementOfferResponse;
        }

        return null;
    }

    public void acceptAdvertisementOffer(String offerId) {
        Optional<Offer> offer = offerRepository.findById(Long.parseLong(offerId));
        Optional<User> user = userService.getCurrentLoggedInUser();

        if (offer.isPresent()) {
            offer.get().setOfferType(OfferType.ACCEPTED);

            Delivery delivery = new Delivery();
            delivery.setUser(user.orElse(null));
            delivery.setForUser(offer.get().getCreatedBy());
            delivery.setOffer(offer.get());
            delivery.setDeliveryType(DeliveryType.DRIVING); //uvijek je driving, pretrazuje se po forUser
            delivery.setDeliveryStatus(DeliveryStatus.INCOMING);
            delivery.setCreatedOn(LocalDate.now());
            delivery.setDeliveryDate(offer.get().getAdvertisement().getDeliveryDate());

            deliveryRepository.save(delivery);
        }

        offerRepository.save(offer.orElse(null));
    }

    public void declineAdvertisementOffer(String offerId) {
        Optional<Offer> offer = offerRepository.findById(Long.parseLong(offerId));

        offer.ifPresent(value -> value.setOfferType(OfferType.DECLINED));

        offerRepository.save(offer.orElse(null));
    }

    public List<OfferResponse> getUserOffers() {
        Optional<User> user = userService.getCurrentLoggedInUser();

        if (user.isPresent()) {
            List<Offer> offers = offerRepository.findAllByCreatedBy(user.get());
            return offerMapper.offerListToOfferResponseList(offers);
        }
        return Collections.emptyList();
    }
}
