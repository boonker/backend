package hr.riteh.boonker.controller;

import hr.riteh.boonker.controller.response.DeliveryResponse;
import hr.riteh.boonker.service.DeliveryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/boonker/api/deliveries")
public class DeliveryController {

    private final DeliveryService deliveryService;

    @GetMapping("/{deliveryId}")
    @PreAuthorize("hasRole('USER')")
    public DeliveryResponse getDelivery(@PathVariable String deliveryId) {
        return deliveryService.getDelivery(deliveryId);
    }

    @GetMapping("/activate/{deliveryId}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> setActiveDelivery(@PathVariable String deliveryId) {
        deliveryService.setActiveDelivery(deliveryId);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/finish/{deliveryId}")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> setFinishedDelivery(@PathVariable String deliveryId) {
        deliveryService.setFinishedDelivery(deliveryId);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/driving")
    @PreAuthorize("hasRole('USER')")
    public List<DeliveryResponse> getDrivingDeliveriesResponse() {
        return deliveryService.getDrivingDeliveriesResponse();
    }

    @GetMapping("/incoming")
    @PreAuthorize("hasRole('USER')")
    public List<DeliveryResponse> getIncomingDeliveriesResponse() {
        return deliveryService.getIncomingDeliveriesResponse();
    }
}
