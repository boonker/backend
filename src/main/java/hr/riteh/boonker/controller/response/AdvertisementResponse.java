package hr.riteh.boonker.controller.response;

import hr.riteh.boonker.model.AdvertisementType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdvertisementResponse {

    private String id;

    private String deliveryDate;

    private String createdAt;

    private String cityFrom;

    private String cityFromAddress;

    private String cityToAddress;

    private String cityTo;

    private String packageSize;

    private String title;

    private String content;

    private String price;

    private String userId;

    private String userFullName;

    private String userPhoneNumber;

    private AdvertisementType advertisementType;
}
