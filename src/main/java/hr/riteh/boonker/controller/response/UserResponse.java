package hr.riteh.boonker.controller.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponse {

    private Long id;

    private String fullName;

    private String username;

    private String email;

    private String city;

    private String age;

    private String phoneNumber;

    private Boolean isFollowed;

    private String totalRatingScore;

    private String totalFollowersNumber;

    private String totalFollowingNumber;
}
