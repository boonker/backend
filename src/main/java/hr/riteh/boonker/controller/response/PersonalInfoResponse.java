package hr.riteh.boonker.controller.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PersonalInfoResponse {

    private String fullName;

    private String username;

    private String email;

    private String city;

    private String phoneNumber;

    private String age;
}
