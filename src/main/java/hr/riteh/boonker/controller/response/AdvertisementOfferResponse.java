package hr.riteh.boonker.controller.response;

import hr.riteh.boonker.model.AdvertisementType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AdvertisementOfferResponse {

    private List<OfferResponse> offers;

    private Long reservedBoonkerSize;

    private Long advertisedBoonkerSize;

    private AdvertisementType advertisementType;
}
