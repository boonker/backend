package hr.riteh.boonker.controller.response;

import hr.riteh.boonker.model.DeliveryStatus;
import hr.riteh.boonker.model.DeliveryType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryResponse {

    private String id;

    private String createdOn;

    private String deliveryDate;

    private String userId;

    private String userFullName;

    private String userPhoneNumber;

    private String forUserId;

    private String forUserFullName;

    private String forUserPhoneNumber;

    private DeliveryType deliveryType;

    private DeliveryStatus deliveryStatus;

    private String cityFrom;

    private String cityFromAddress;

    private String cityTo;

    private String cityToAddress;

    private String price;

    private String message;
}
