package hr.riteh.boonker.controller.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFollowResponse {

    private UserResponse userResponse;

    private Boolean isFollowedByCurrentUser;
}
