package hr.riteh.boonker.controller.response;

import hr.riteh.boonker.model.OfferType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfferResponse {

    private Long id;

    private Long packageSize;

    private String message;

    private String publishedOn;

    private String address;

    private String createdById;

    private OfferType offerType;

    private String createdByFullName;

    private String advertisementId;
}
