package hr.riteh.boonker.controller.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpoPushTokenResponse {

    public String userId;

    public String userFullName;

    public String expoPushToken;
}
