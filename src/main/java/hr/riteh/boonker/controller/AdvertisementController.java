package hr.riteh.boonker.controller;

import hr.riteh.boonker.controller.request.NewAdvertisementRequest;
import hr.riteh.boonker.controller.request.SearchAdvertisementQueryRequest;
import hr.riteh.boonker.controller.response.AdvertisementResponse;
import hr.riteh.boonker.controller.response.PersonalInfoResponse;
import hr.riteh.boonker.model.Advertisement;
import hr.riteh.boonker.model.User;
import hr.riteh.boonker.repository.AdvertisementRepository;
import hr.riteh.boonker.service.AdvertisementService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/boonker/api/advertisements")
public class AdvertisementController {

    private final AdvertisementService advertisementService;

    private final AdvertisementRepository advertisementRepository;

    /**
     * Create new advertisment for logged in user
     *
     * @param newAdvertisementRequest
     * @return
     */
    @PostMapping("/new")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> newAdvertisement(@RequestBody NewAdvertisementRequest newAdvertisementRequest) {
        advertisementService.newAdvertisement(newAdvertisementRequest);

        return ResponseEntity.ok("OK");
    }

    @DeleteMapping("/{id}/delete")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> deleteAdvertisement(@PathVariable Long id) {
        advertisementService.deleteAdvertisement(id);
        return ResponseEntity.ok("OK");
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    public AdvertisementResponse getAdvertisement(@PathVariable Long id) {
        return advertisementService.getAdvertisement(id);
    }

    @PutMapping("/{id}/update")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> updateAdvertisement(@PathVariable Long id, @RequestBody NewAdvertisementRequest newAdvertisementRequest) {
        advertisementService.updateAdvertisement(id, newAdvertisementRequest);
        return ResponseEntity.ok("OK");
    }

    @PostMapping("/all")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<List<AdvertisementResponse>> getAllAdvertisements(@Nullable @RequestBody SearchAdvertisementQueryRequest searchAdvertisementQueryRequest) {
        return ResponseEntity.ok(advertisementService.getAllAdvertisements(searchAdvertisementQueryRequest));
    }

    @GetMapping("/my")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<List<Advertisement>> getAllUserAdvertisements() {
        return ResponseEntity.ok(advertisementService.allUserAdvertisement());
    }
    /**
     * Retrives all advertisments in database
     *
     * @return
     */
    @GetMapping("/allAds")
    @PreAuthorize("hasRole('USER')")
    public List<Advertisement> all() {
        return advertisementRepository.findAll();
    }
}
