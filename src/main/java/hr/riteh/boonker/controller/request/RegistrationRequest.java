package hr.riteh.boonker.controller.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class RegistrationRequest {

    private String fullName;

    @Size(min = 3, max = 30)
    private String username;

    @Size(max = 50)
    @Email
    private String email;

    private String password;

    private String city;

    private String age;

    @Nullable
    @Size(max = 30)
    private String phoneNumber;

    private Set<String> role;
}
