package hr.riteh.boonker.controller.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewOfferRequest {

    private String advertisementId;

    private String packageSize;

    private String message;

    private String address;
}
