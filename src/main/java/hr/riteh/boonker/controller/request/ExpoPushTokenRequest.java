package hr.riteh.boonker.controller.request;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class ExpoPushTokenRequest {

    @NotBlank
    private String expoPushToken;
}
