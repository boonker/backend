package hr.riteh.boonker.controller.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdatePersonalInfo {

    private String fullName;

    private String password;

    private String city;

    private String phoneNumber;

    private String age;
}
