package hr.riteh.boonker.controller.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class NewAdvertisementRequest {

    private String title;

    private String content;

    private String price;

   private String deliveryDate;

   private String cityFrom;

    private String cityFromAddress;

   private String cityTo;

    private String cityToAddress;

   private String packageSize;

   private String advertisementType;
}

