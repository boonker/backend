package hr.riteh.boonker.controller.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchAdvertisementQueryRequest {

    private String cityFrom;

    private String cityTo;

    private String deliveryDate;

    private String advertisementType;

    private String sortBy;

    private String userFullName;

    private Boolean followedByUser;
}
