package hr.riteh.boonker.controller.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewRatingRequest {

    private String forUserId;

    private Long score;

    private String review;
}
