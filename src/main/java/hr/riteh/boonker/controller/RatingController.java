package hr.riteh.boonker.controller;

import hr.riteh.boonker.controller.request.NewRatingRequest;
import hr.riteh.boonker.model.Rating;
import hr.riteh.boonker.service.RatingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/boonker/api/ratings")
public class RatingController {

    private final RatingsService ratingsService;

    @GetMapping("/user/{userId}")
    @PreAuthorize("hasRole('USER')")
    public List<Rating> getUserRatings(@PathVariable String userId) {
        return ratingsService.getUserRatings(Long.valueOf(userId));
    }

    @PostMapping("/new")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> createUserRating(@RequestBody NewRatingRequest newRatingRequest) {
        ratingsService.newUserRatings(newRatingRequest);

        return ResponseEntity.ok("OK");
    }
}
