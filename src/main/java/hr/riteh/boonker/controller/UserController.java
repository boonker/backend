package hr.riteh.boonker.controller;

import hr.riteh.boonker.controller.request.ExpoPushTokenRequest;
import hr.riteh.boonker.controller.request.UpdatePersonalInfo;
import hr.riteh.boonker.controller.response.*;
import hr.riteh.boonker.model.User;
import hr.riteh.boonker.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/boonker/api/users")
public class UserController {

    private final UserService userService;

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    public UserResponse getUserDetails(@PathVariable String id) {
        return userService.getUserDetails(id);
    }

    @GetMapping("/{id}/advertisement")
    @PreAuthorize("hasRole('USER')")
    public List<AdvertisementResponse> getUserAdvertisement(@PathVariable String id) {
        return userService.getUserAdvertisement(id);
    }

    @GetMapping("/{id}/follow")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> followUser(@PathVariable String id) {
        userService.followUser(id);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/{id}/unfollow")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> unfollowUser(@PathVariable String id) {
        userService.unfollowUser(id);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/{id}/followers")
    @PreAuthorize("hasRole('USER')")
    public List<UserFollowResponse> getUserFollowers(@PathVariable String id) {
        return userService.getUserFollowers(id);
    }

    @GetMapping("/{id}/following")
    @PreAuthorize("hasRole('USER')")
    public List<UserFollowResponse> getUserFollowing(@PathVariable String id) {
        return userService.getUserFollowing(id);
    }

    @PostMapping("/expo")
    public void setExpoPushToken(@RequestBody ExpoPushTokenRequest expoPushTokenRequest) {
        userService.setExpoPushToken(expoPushTokenRequest);
    }

    @GetMapping("/expo/{id}")
    public ResponseEntity<ExpoPushTokenResponse> getExpoPushToken(@PathVariable String id) {
        return ResponseEntity.ok(userService.getExpoPushToken(id));
    }


    @GetMapping("/info")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<PersonalInfoResponse> getCurrentLoggedInUserInfo() {
        return ResponseEntity.ok(userService.getPersonalInfoResponse());
    }

    @PutMapping("/info")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> updateCurrentLoggedInUserInfo(@RequestBody UpdatePersonalInfo updatePersonalInfo) {
        userService.updatePersonalInfo(updatePersonalInfo);
        return ResponseEntity.ok("OK");
    }
}
