package hr.riteh.boonker.controller;

import hr.riteh.boonker.controller.request.NewOfferRequest;
import hr.riteh.boonker.controller.response.AdvertisementOfferResponse;
import hr.riteh.boonker.controller.response.OfferResponse;
import hr.riteh.boonker.service.OfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/boonker/api/offers")
public class OfferController {

    private final OfferService offerService;

    @PostMapping("/new")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> newOffer(@RequestBody NewOfferRequest newOfferRequest) {
        offerService.newOffer(newOfferRequest);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/{advertisementId}")
    @PreAuthorize("hasRole('USER')")
    public AdvertisementOfferResponse getAdvertisementOffers(@PathVariable String advertisementId) {

        return offerService.getAdvertisementOffers(advertisementId);
    }

    @GetMapping("/{offerId}/accept")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> acceptAdvertisementOffer(@PathVariable String offerId) {
        offerService.acceptAdvertisementOffer(offerId);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/{offerId}/decline")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<String> declineAdvertisementOffer(@PathVariable String offerId) {
        offerService.declineAdvertisementOffer(offerId);

        return ResponseEntity.ok("OK");
    }

    @GetMapping("/my")
    public ResponseEntity<List<OfferResponse>> getUserOffers() {
        return ResponseEntity.ok(offerService.getUserOffers());
    }
}
