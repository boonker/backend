package hr.riteh.boonker.model;

public enum DeliveryStatus {
    INCOMING,
    TODAY,
    ACTIVE,
    FINISHED,
    PAST,
}
