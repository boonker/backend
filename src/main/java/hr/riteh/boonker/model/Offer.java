package hr.riteh.boonker.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "offers")
public class Offer {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "published_on")
    private LocalDate publishedOn;

    @Column(name = "packageSize")
    private Long packageSize;

    @Column(name = "message")
    private String message;

    @Column(name = "address")
    private String address;

    @Enumerated(EnumType.STRING)
    @Column(name = "offer_type")
    private OfferType offerType;

    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    @OneToOne()
    private Advertisement advertisement;
}
