package hr.riteh.boonker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "rating")
public class Rating {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private @Id
    Long id;

    @Column(name="published_on")
    LocalDate publishedOn;

    @Column(name = "score")
    Long score;

    @Column(name = "review")
    String review;

    @ManyToOne()
    private User createdBy;

    @JsonIgnore
    @ManyToOne()
    private User createdFor;
}
