package hr.riteh.boonker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        })
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @NotBlank
    @Size(max = 30)
    @Column(name = "username")
    private String username;

    @Size(max = 50)
    @Email
    @Column(name = "email")
    private String email;

    @JsonIgnore
    @NotBlank
    @Column(name = "password")
    private String password;

    @Column(name = "city")
    private String city;

    @Column(name = "age")
    private String age;

    @Nullable
    @Size(max = 30)
    @Column(name = "phone_number")
    private String phoneNumber;

    @Column
    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    private List<User> following;

    @Column
    @ManyToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    private List<User> followers;

    @Column(name="expo_push_token")
    private String expoPushToken;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    public User(String fullName, String username, String email, String password,
                String city, String age,
                @Nullable String phoneNumber) {
        this.fullName = fullName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.city = city;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }
}
