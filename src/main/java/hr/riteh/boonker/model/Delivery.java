package hr.riteh.boonker.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "deliveries")
public class Delivery {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "created_on")
    private LocalDate createdOn;

    @Column(name = "delivery_date")
    private String deliveryDate;

    @OneToOne()
    private Offer offer;

    @ManyToOne()
    private User user;

    @ManyToOne()
    private User forUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "delivery_type")
    private DeliveryType deliveryType;

    @Enumerated(EnumType.STRING)
    @Column(name = "delivery_status")
    private DeliveryStatus deliveryStatus;

}
