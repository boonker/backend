package hr.riteh.boonker.model;

public enum DeliveryType {
    DRIVING,
    SENDING,
}
