package hr.riteh.boonker.model;

public enum RoleType {
    ROLE_USER,
    ROLE_ADMIN
}
