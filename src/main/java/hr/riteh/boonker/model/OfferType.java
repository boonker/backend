package hr.riteh.boonker.model;

public enum OfferType {
    ACCEPTED,
    PENDING,
    DECLINED,
    CANCELED
}
