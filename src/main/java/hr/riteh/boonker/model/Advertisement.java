package hr.riteh.boonker.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;

@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "advertisement")
public class Advertisement {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name = "created_at")
    private String createdAt;

    @Column(name = "delivery_date")
    private String deliveryDate;

    @Column(name = "city_from")
    private String cityFrom;

    @Column(name = "city_from_address")
    private String cityFromAddress;

    @Column(name = "city_to")
    private String cityTo;

    @Column(name = "city_to_address")
    private String cityToAddress;

    @Column(name = "package_size")
    private String packageSize;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "price")
    private Long price;

    @Enumerated(EnumType.STRING)
    @Column(name = "advertisement_type")
    private AdvertisementType advertisementType;

    @ManyToOne(cascade = CascadeType.REMOVE)
    private User user;

    @Column(name = "is_deleted", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean isDeleted;

    @JsonIgnore
    @OneToMany(mappedBy = "id", cascade = CascadeType.REMOVE)
    private Collection<Offer> offer;
}
