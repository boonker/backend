package hr.riteh.boonker.model;

public enum SortAdvertisement {
    LOW_TO_HIGH_PRICE,
    HIGH_TO_LOW_PRICE
}
