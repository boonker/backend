package hr.riteh.boonker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoonkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoonkerApplication.class, args);
    }

}
